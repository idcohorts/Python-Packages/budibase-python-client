We thank all contributors of this project!

# Developers
- Nick Schulze (Working Group Cohorts in Infectious Diseases and Oncology; University Hospital Cologne)
- Elias Hamacher (Working Group Cohorts in Infectious Diseases and Oncology; University Hospital Cologne)
- Markus Katharina Brechtel (Working Group Cohorts in Infectious Diseases and Oncology; University Hospital Cologne / University Hospital Frankfurt)
